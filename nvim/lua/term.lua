require 'toggleterm'.setup {
  shell = "/bin/zsh",
  direction = "float",
  size = 20,
}

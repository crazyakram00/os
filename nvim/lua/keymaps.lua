local opts   = { silent = true, noremap = true }
local keymap = vim.api.nvim_set_keymap
keymap('', '<Space>', '<Nop>', opts)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
keymap('n', '<C-h>', '<C-w>h', opts)
keymap('n', '<C-j>', '<C-w>j', opts)
keymap('n', '<C-k>', '<C-w>k', opts)
keymap('n', '<C-l>', '<C-w>l', opts)
keymap('n', '<leader>e', '<Cmd>lua require(\'lir.float\').toggle() <cr>', opts)
keymap('n', '<leader>f', '<Cmd> Telescope find_files <cr>', opts)
keymap('t', '<C-d>', '<Cmd> ToggleTerm <cr>', opts)
keymap('n', '<leader>th', '<Cmd> ToggleTerm direction=horizontal=20 <cr>', { noremap = true })
keymap('n', '<leader>tv', '<Cmd> ToggleTerm direction=vertical size=60 <cr>', { noremap = true })
keymap('n', '<leader>tf', '<Cmd> ToggleTerm direction=float size=40 <cr>', { noremap = true })
keymap('n', '<S-h>', ':bprevious <cr>', opts)
keymap('n', '<S-l>', ':bnext <cr>', opts)
keymap('n', '<C-s>', ':w <cr>', opts)
keymap('n', '<leader>sv', '<Cmd> vsplit <cr>', opts)
keymap('n', '<leader>sh', '<Cmd> split <cr>', opts)

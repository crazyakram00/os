local sumneko_luapath = vim.fn.exepath('/home/moon/.config/nvim/lang-servers/lua-language-server/bin/lua-language-server')
vim.diagnostic.config({
  virtual_text = true,
  signs = true,
  underline = true,
  update_in_insert = false,
  severity_sort = false,
})
vim.cmd [[autocmd BufWritePre * lua vim.lsp.buf.formatting_sync()]]
local lsp = require 'lspconfig'
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)
lsp.tsserver.setup {
  filetypes = { "typescript", "javascript", "typescriptreact", "javascriptreact" },
  capabilities = capabilities,
  --  cmd = { vim.fn.exepath('/bin/typescript-language-server') },
}
lsp.html.setup {
  filetypes = { "html", "typescriptreact", "javascriptreact" },
  capabilities = capabilities,
}
lsp.cssls.setup {
  filetypes = { "css" },
  capabilities = capabilities,
}
lsp.sumneko_lua.setup {
  filetypes = { "lua" },
  capabilities = capabilities,
  cmd = { sumneko_luapath },
}
lsp.rust_analyzer.setup {
  filetypes = { "rust" },
  capabilities, capabilities,
}
lsp.emmet_ls.setup {
  filetypes = { "html", "typescriptreact", "javascriptreact" },
  capabilities = capabilities,
}
lsp.clangd.setup {
  filetypes = { "cpp", "c" },
  capabilities = capabilities,
}
lsp.pyright.setup {
  filetypes = { "python" },
  capabilities = capabilities,
}
lsp.texlab.setup {
  capabilities = capabilities,
  filetypes = { "plaintex", "tex", "bib" },
}
lsp.grammarly.setup {
  filetypes = { "tex", "bib", "plaintex", "markdown" },
  capabilities = capabilities,
}
lsp.jsonls.setup {
  filetypes = { "json" },
  capabilities = capabilities,
}
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

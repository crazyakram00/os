require 'lir'.setup {
  devicons_enable = true,
  mappings = {
    ['l'] = require 'lir.actions'.edit,
    ['h'] = require 'lir.actions'.up,
    ['q'] = require 'lir.float'.toggle,
    ['r'] = require 'lir.actions'.rename,
    ['n'] = require 'lir.actions'.newfile,
    ['d'] = require 'lir.actions'.mkdir,
    ['x'] = require 'lir.actions'.delete,
    ['.'] = require 'lir.actions'.toggle_show_hidden,
  },
}

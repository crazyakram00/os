local opt = vim.opt

-- options
opt.number = true
opt.relativenumber = true
opt.clipboard = 'unnamedplus'
opt.undofile = true
opt.smarttab = true
opt.splitbelow = true
opt.splitright = true
opt.cursorline = true
opt.background = 'dark'
opt.encoding = 'utf-8'
opt.expandtab = true
opt.shiftwidth = 2
opt.tabstop = 2
opt.pumheight = 10
opt.termguicolors = true
opt.guifont = 'FiraCode Nerd Font:h7'

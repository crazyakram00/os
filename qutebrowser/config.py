config.load_autoconfig()
config.set("colors.webpage.darkmode.enabled", True)
config.source('./base16-qutebrowser/themes/default/base16-solarized-dark.config.py')
c.downloads.remove_finished = 3000;
c.url.searchengines = {"DEFAULT": "https://searx.be/search?q={}"}
c.url.start_pages = "file:///home/moon/.sys/startpage/index.html"
c.url.default_page = "file:///home/moon/.sys/startpage/index.html"
c.completion.show = "auto"
c.colors.webpage.preferred_color_scheme = "dark"
config.bind("M","hint links spawn youtube-viewer --resolution=720p {hint-url}")
c.fonts.default_family = "JetBrains Mono ExtraBold"
c.fonts.default_size = "10pt"
c.fonts.web.family.cursive = "JetBrainsMono Nerd Font"
c.fonts.web.family.fantasy = "JetBrainsMono Nerd Font"
c.fonts.web.family.fixed = "JetBrainsMono Nerd Font"
c.fonts.web.family.sans_serif = "JetBrainsMono Nerd Font"
c.fonts.web.family.standard = "JetBrainsMono Nerd Font"
c.fonts.web.family.serif = "JetBrainsMono Nerd Font"
c.content.user_stylesheets = [
    'solarized-dark.css',
    'custom_solarized.css'
]
